import cv2
import tkinter
from PIL import ImageTk, Image
import time
import Person
import numpy as np
from MyVideoCapture import MyVideoCapture

class App:

    def updadeSettings(self):
        self.vid.UpdateSettings(self.sld_leftLimit.get(), self.sld_rightLimit.get(), self.sld_leftLine.get(),
                                self.sld_rightLine.get(), self.sld_maxArea.get(), self.sld_minArea.get(),
                                self.inp_path.get(), self.vertical)

    def rotate(self):
        if self.vertical:
            self.vertical = False
        else:
            self.vertical = True
        self.updadeSettings()

    def saveSettings(self):
        try:
            self.settings = open('settings.txt', "w")
            self.settings.write(
                str(self.sld_leftLimit.get()) + " " +
                str(self.sld_rightLimit.get()) + " " +
                str(self.sld_leftLine.get()) + " " +
                str(self.sld_rightLine.get()) + " " +
                str(self.sld_maxArea.get()) + " " +
                str(self.sld_minArea.get()) + " " +
                str(self.inp_path.get()) + " " +
                str(self.vertical))

            self.settings.close()
        except:
            print("No se puede abrir el archivo log")

    def loadSettings(self):
        try:
            self.settings = open('settings.txt', "r")
            str_settings = self.settings.read().split(" ")
            self.sld_leftLimit.set(int(str_settings[0]))
            self.sld_rightLimit.set(int(str_settings[1]))
            self.sld_leftLine.set(int(str_settings[2]))
            self.sld_rightLine.set(int(str_settings[3]))
            self.sld_maxArea.set(int(str_settings[4]))
            self.sld_minArea.set(int(str_settings[5]))
            self.inp_path.insert(0, str_settings[6])
            self.path = str_settings[6]
            print(self.path)
            if str_settings[7] == "True":
                self.vertical = True
            else:
                self.vertical = False
            self.settings.close()
        except:
            print("Problema ao abrir configurações")

    def __init__(self, window, window_title):

        self.window = window
        self.window.title(window_title)

        self.lateralFrame = tkinter.Frame(self.window)
        self.lateralFrame.grid(row=0, column=2)

        self.btn_frame = tkinter.Frame(self.lateralFrame)
        self.btn_frame.grid(row=8, column=1, pady=10)

        self.sld_leftLimit = tkinter.Scale(self.lateralFrame, from_=0, to=100, orient=tkinter.HORIZONTAL, length=400)
        self.sld_rightLimit = tkinter.Scale(self.lateralFrame, from_=0, to=100, orient=tkinter.HORIZONTAL, length=400)
        self.sld_leftLine = tkinter.Scale(self.lateralFrame, from_=0, to=100, orient=tkinter.HORIZONTAL, length=400)
        self.sld_rightLine = tkinter.Scale(self.lateralFrame, from_=0, to=100, orient=tkinter.HORIZONTAL, length=400)
        self.sld_maxArea = tkinter.Scale(self.lateralFrame, from_=0, to=100, orient=tkinter.HORIZONTAL, length=400)
        self.sld_minArea = tkinter.Scale(self.lateralFrame, from_=0, to=100, orient=tkinter.HORIZONTAL, length=400)

        self.lbl_rightLimit = tkinter.Label(self.lateralFrame, text="Right Limit")
        self.lbl_leftLimit = tkinter.Label(self.lateralFrame, text="Left Limit")
        self.lbl_rightLine = tkinter.Label(self.lateralFrame, text="In Line")
        self.lbl_leftLine = tkinter.Label(self.lateralFrame, text="Out Line")
        self.lbl_maxArea = tkinter.Label(self.lateralFrame, text="Max Area")
        self.lbl_minArea = tkinter.Label(self.lateralFrame, text="Min Area")

        self.btn_snapshot = tkinter.Button(self.btn_frame, text="Snapshot", command=self.snapshot)
        self.btn_updadeSettings = tkinter.Button(self.btn_frame, text="Updade", command=self.updadeSettings)
        self.btn_saveSettings = tkinter.Button(self.btn_frame, text="Save", command=self.saveSettings)
        self.btn_Rotate = tkinter.Button(self.btn_frame, text="Rotate", command=self.rotate)

        self.lbl_Path = tkinter.Label(self.lateralFrame, text="Video Path")
        self.lbl_Path.grid(row=1, column=0, sticky=tkinter.W, padx=10)

        self.inp_path = tkinter.Entry(self.lateralFrame)
        self.inp_path.grid(row=1, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.lbl_rightLimit.grid(row=2, column=0, sticky=tkinter.W + tkinter.S, padx=10)
        self.sld_rightLimit.grid(row=2, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.lbl_leftLimit.grid(row=3, column=0, sticky=tkinter.W + tkinter.S, padx=10)
        self.sld_leftLimit.grid(row=3, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.lbl_rightLine.grid(row=4, column=0, sticky=tkinter.W + tkinter.S, padx=10)
        self.sld_rightLine.grid(row=4, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.lbl_leftLine.grid(row=5, column=0, sticky=tkinter.W + tkinter.S, padx=10)
        self.sld_leftLine.grid(row=5, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.lbl_maxArea.grid(row=6, column=0, sticky=tkinter.W + tkinter.S, padx=10)
        self.sld_maxArea.grid(row=6, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.lbl_minArea.grid(row=7, column=0, sticky=tkinter.W + tkinter.S, padx=10)
        self.sld_minArea.grid(row=7, column=1, sticky=tkinter.W + tkinter.E, padx=10)

        self.btn_snapshot.grid(row=0, column=0, padx=10)
        self.btn_updadeSettings.grid(row=0, column=1, sticky=tkinter.W + tkinter.E, padx=10)
        self.btn_saveSettings.grid(row=0, column=2, sticky=tkinter.W + tkinter.E, padx=10)
        self.btn_Rotate.grid(row=0, column=3, sticky=tkinter.W + tkinter.E, padx=10)

        self.vertical = True

        self.loadSettings()

        try:
            self.path = int(self.inp_path.get())
            self.vid = MyVideoCapture(self.sld_leftLimit.get(), self.sld_rightLimit.get(),
                                      self.sld_leftLine.get(), self.sld_rightLine.get(), self.sld_maxArea.get(),
                                      self.sld_minArea.get(), self.path, self.vertical)
            self.canvas = tkinter.Canvas(self.window, width=self.vid.width, height=self.vid.height)
            self.canvas.grid(row=0, column=0, pady=10)
            self.path_found = True
            print("Success")
        except:
            try:
                self.vid = MyVideoCapture(self.sld_leftLimit.get(), self.sld_rightLimit.get(),
                                          self.sld_leftLine.get(), self.sld_rightLine.get(), self.sld_maxArea.get(),
                                          self.sld_minArea.get(), self.path, self.vertical)
                self.canvas = tkinter.Canvas(window, width=self.vid.width, height=self.vid.height)
                self.canvas.grid(row=0, column=0, pady=10)
                self.path_found = True
                print("Success")
            except:
                self.path_found = False
                print("Fail to Load Video")
                # self.path = self.inp_path.get()
                # self.inp_path.delete(0, "end")
                # self.inp_path.insert(0, "Path not Found: " + self.inp_path.get())

        self.delay = 15
        self.update()
        self.window.mainloop()

    def snapshot(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()
        if ret:
            cv2.imwrite("./Pics/"
                        "frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))

    # Main Loop
    def update(self):
        if self.path_found:
            ret, frame = self.vid.get_frame()
            if ret:
                self.photo = ImageTk.PhotoImage(image=Image.fromarray(frame))
                self.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)
        self.window.after(self.delay, self.update)


App(tkinter.Tk(), window_title="PeopleCounter")
