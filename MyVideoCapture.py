import tkinter
import cv2
from PIL import ImageTk, Image
import time
import Person
import numpy as np


class MyVideoCapture:
    # Variables
    font = cv2.FONT_HERSHEY_SIMPLEX
    persons = []
    max_p_age = 5
    pid = 1

    def UpdateSettings(self, leftLimit, rightLimit, leftLine, rightLine, maxArea, minArea, video_source, vertical):

        if vertical:
            self.left_limit = int(leftLimit * (self.width / 100))
            self.right_limit = int(rightLimit * (self.width / 100))
            self.line_left = int(leftLine * (self.width / 100))
            self.line_right = int(rightLine * (self.width / 100))
        else:
            self.left_limit = int(leftLimit * (self.height / 100))
            self.right_limit = int(rightLimit * (self.height / 100))
            self.line_left = int(leftLine * (self.height / 100))
            self.line_right = int(rightLine * (self.height / 100))

        self.maxAreaTH = int(maxArea * (self.height * self.width) / 100)
        self.minAreaTH = int(minArea * (self.height * self.width) / 200)
        self.video_source = video_source
        self.vertical = vertical

    def __init__(self, leftLimit=0, rightLimit=0, leftLine=0, rightLine=0, maxArea=0, minArea=0, video_source='localhost',
                 vertical=False):
        self.cnt_right = 0
        self.cnt_left = 0
        self.cnt_inside = 0
        self.video_source = video_source
        self.vertical = vertical

        # Open the video source

        self.vid = cv2.VideoCapture(self.video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", self.video_source)
        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

        self.vid.set(10,150)

        self.frameArea = self.height * self.width

        if vertical:
            self.left_limit = int(leftLimit * (self.width / 100))
            self.right_limit = int(rightLimit * (self.width / 100))
            self.line_left = int(leftLine * (self.width / 100))
            self.line_right = int(rightLine * (self.width / 100))
        else:
            self.left_limit = int(leftLimit * (self.height / 100))
            self.right_limit = int(rightLimit * (self.height / 100))
            self.line_left = int(leftLine * (self.height / 100))
            self.line_right = int(rightLine * (self.height / 100))

        self.maxAreaTH = int(maxArea * (self.height * self.width) / 100)
        self.minAreaTH = int(minArea * (self.height * self.width) / 200)

        self.line_left_color = (0, 0, 255)
        self.line_right_color = (255, 0, 0)

        self.fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=True, varThreshold=1000)  # Best Result
        # self.fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=True, varThreshold=4)  # Best Result

        self.kernelOp = np.ones((1, 1), np.uint8)
        self.kernelOp2 = np.ones((1, 1), np.uint8)
        self.kernelCl = np.ones((20, 20), np.uint8)

    # This Function returns a frame of the video
    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()

            if ret:
                # Code from the other code goes here

                for i in self.persons:
                    i.age_one()  # age every person one frame

                fgmask = self.fgbg.apply(frame)
                fgmask2 = self.fgbg.apply(frame)

                try:
                    ret, imBin = cv2.threshold(fgmask, 254, 255, cv2.THRESH_BINARY)
                    ret, imBin2 = cv2.threshold(fgmask2, 254, 255, cv2.THRESH_BINARY)
                    # Opening (erode->dilate) para quitar ruido.
                    mask = cv2.morphologyEx(imBin, cv2.MORPH_OPEN, self.kernelOp)
                    mask2 = cv2.morphologyEx(imBin2, cv2.MORPH_OPEN, self.kernelOp)
                    # Closing (dilate -> erode) para juntar regiones blancas.
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, self.kernelCl)
                    mask2 = cv2.morphologyEx(mask2, cv2.MORPH_CLOSE, self.kernelCl)

                except:
                    print('EOF')
                    print('UP:', self.cnt_right)
                    print('DOWN:', self.cnt_left)

                contours0, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

                for cnt in contours0:
                    area = cv2.contourArea(cnt)
                    if self.minAreaTH < area < self.maxAreaTH:
                        #################
                        #   TRACKING    #
                        #################

                        M = cv2.moments(cnt)
                        centerx = int(M['m10'] / M['m00'])
                        centery = int(M['m01'] / M['m00'])
                        x, y, w, h = cv2.boundingRect(cnt)
                        new = True

                        if self.vertical:
                            self.center = centerx
                        else:
                            self.center = centery

                        if self.center in range(self.left_limit, self.right_limit):
                            for i in self.persons:
                                if abs(x - i.getX()) <= w and abs(y - i.getY()) <= h:
                                    # el objeto esta cerca de uno que ya se detecto antes
                                    new = False
                                    i.updateCoords(centerx,
                                                   centery)  # actualiza coordenadas en el objeto and resets age
                                    if self.vertical:
                                        if self.line_left < self.line_right:
                                            if i.going_RIGHT(self.line_right):
                                                self.cnt_right += 1
                                                self.cnt_inside += 1
                                                print("ID:", i.getId(), 'crossed going right at', time.strftime("%c"))
                                                cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                            elif i.going_LEFT(self.line_left):
                                                self.cnt_left += 1
                                                if self.cnt_inside > 0:
                                                    self.cnt_inside -= 1
                                                print("ID:", i.getId(), 'crossed going left at', time.strftime("%c"))
                                                cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                        else:
                                            if i.going_RIGHT(self.line_left):
                                                self.cnt_left += 1
                                                if self.cnt_inside > 0:
                                                    self.cnt_inside -= 1
                                                print("ID:", i.getId(), 'crossed going right at', time.strftime("%c"))
                                                cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                            elif i.going_LEFT(self.line_right):
                                                self.cnt_right += 1
                                                self.cnt_inside += 1
                                                print("ID:", i.getId(), 'crossed going left at', time.strftime("%c"))
                                                cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                        if i.getState() == '1':
                                            if i.getDir() == 'left' and i.getX() > self.right_limit:
                                                i.setDone()
                                            elif i.getDir() == 'right' and i.getX() < self.left_limit:
                                                i.setDone()
                                    else:
                                        if self.line_left < self.line_right:
                                            if i.going_UP(self.line_left):
                                                self.cnt_left += 1
                                                if self.cnt_inside > 0:
                                                    self.cnt_inside -= 1
                                                    cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                                cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                            elif i.going_DOWN(self.line_right):
                                                self.cnt_right += 1
                                                self.cnt_inside += 1
                                                cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                        else:
                                            if i.going_UP(self.line_right):
                                                self.cnt_right += 1
                                                self.cnt_inside += 1
                                                cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                            elif i.going_DOWN(self.line_left):
                                                self.cnt_left += 1
                                                if self.cnt_inside > 0:
                                                    self.cnt_inside -= 1
                                                    cv2.imwrite("./Pics/frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg",
                                                                cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
                                        if i.getState() == '1':
                                            if i.getDir() == 'up' and i.getY() > self.right_limit:
                                                i.setDone()
                                            elif i.getDir() == 'down' and i.getY() < self.left_limit:
                                                i.setDone()
                            if new:
                                p = Person.MyPerson(self.pid, centerx, centery, self.max_p_age)
                                self.persons.append(p)
                                self.pid += 1

                        #################
                        #   DIBUJOS     #
                        #################
                        cv2.circle(frame, (centerx, centery), 5, (255, 0, 0), -1)
                        img = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    # cv.drawContours(frame, cnt, -1, (0,255,0), 3)

                # END for cnt in contours0
                for i in self.persons:
                    if i.timedOut():
                        index = self.persons.index(i)
                        self.persons.pop(index)
                        print('removed')
                        del i  # liberar la memoria de i

                    elif len(i.getTracks()) >= 2:
                        pts = np.array(i.getTracks(), np.int32)
                        pts = pts.reshape((-1, 1, 2))
                        cv2.polylines(frame, [pts], False, i.getRGB())
                        cv2.putText(frame, str(i.getId()), (i.getX() + 10, i.getY() + 10), self.font, 0.3, i.getRGB(),
                                    2, cv2.LINE_AA)
                        cv2.putText(frame, str(i.getDirection()), (i.getX() + 10, i.getY() + 10), self.font, 0.3,
                                    i.getRGB(), 1, cv2.LINE_AA)
                #################
                #   IMAGANES    #
                #################
                str_up = 'IN: ' + str(self.cnt_right)
                str_down = 'OUT: ' + str(self.cnt_left)
                str_inside = 'INSIDE: ' + str(self.cnt_inside)

                if self.vertical:
                    # right Line
                    pt1 = [self.line_right, 0]  # x,y pt1 Up
                    pt2 = [self.line_right, self.height]  # x,y pt2 Down
                    pts_L1 = np.array([pt1, pt2], np.int32)
                    pts_L1 = pts_L1.reshape((-1, 1, 2))

                    # Left Line
                    pt3 = [self.line_left, 0]
                    pt4 = [self.line_left, self.height]
                    pts_L2 = np.array([pt3, pt4], np.int32)
                    pts_L2 = pts_L2.reshape((-1, 1, 2))

                    # Right_Limit
                    pt5 = [self.right_limit, 0]
                    pt6 = [self.right_limit, self.height]
                    pts_L3 = np.array([pt5, pt6], np.int32)
                    pts_L3 = pts_L3.reshape((-1, 1, 2))

                    # Left_Limit
                    pt7 = [self.left_limit, 0]
                    pt8 = [self.left_limit, self.height]
                    pts_L4 = np.array([pt7, pt8], np.int32)
                    pts_L4 = pts_L4.reshape((-1, 1, 2))

                else:
                    # right Line
                    pt1 = [0, self.line_right]  # x,y pt1 Left
                    pt2 = [self.width, self.line_right]  # x,y pt2 Right
                    pts_L1 = np.array([pt1, pt2], np.int32)
                    pts_L1 = pts_L1.reshape((-1, 1, 2))

                    # Left Line
                    pt3 = [0, self.line_left]
                    pt4 = [self.width, self.line_left]
                    pts_L2 = np.array([pt3, pt4], np.int32)
                    pts_L2 = pts_L2.reshape((-1, 1, 2))

                    # Right_Limit
                    pt5 = [0, self.right_limit]
                    pt6 = [self.width, self.right_limit]
                    pts_L3 = np.array([pt5, pt6], np.int32)
                    pts_L3 = pts_L3.reshape((-1, 1, 2))

                    # Left_Limit
                    pt7 = [0, self.left_limit]
                    pt8 = [self.width, self.left_limit]
                    pts_L4 = np.array([pt7, pt8], np.int32)
                    pts_L4 = pts_L4.reshape((-1, 1, 2))

                cv2.polylines(frame, [pts_L1], False, self.line_left_color, thickness=3)
                cv2.polylines(frame, [pts_L2], False, self.line_right_color, thickness=3)
                cv2.polylines(frame, [pts_L3], False, (255, 255, 255), thickness=3)
                cv2.polylines(frame, [pts_L4], False, (255, 255, 255), thickness=3)
                cv2.putText(frame, str_up, (10, 20), self.font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, str_up, (10, 20), self.font, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                cv2.putText(frame, str_down, (10, 40), self.font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, str_down, (10, 40), self.font, 0.5, (255, 0, 0), 1, cv2.LINE_AA)
                cv2.putText(frame, str_inside, (10, 60), self.font, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, str_inside, (10, 60), self.font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
                cv2.imshow('Frame', mask)

                return ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            else:
                return ret, None

        # Release the video source when the object is destroyed

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()
        # Create a window and pass it to the Application object
