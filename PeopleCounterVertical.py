import numpy as np
import cv2 as cv
import Person
import time

try:
    log = open('log.txt', "w")
except:
    print("No se puede abrir el archivo log")

# Contadores de entrada y salida
cnt_right = 0
cnt_left = 0
cnt_inside = 0

# Fuente de video
cap = cv.VideoCapture(0)

# cap = cv.MyVideoCapture.py('Test Files/VerticalCounterTest2.mp4')
# camera = PiCamera()
##camera.resolution = (160,120)
##camera.framerate = 5
##rawCapture = PiRGBArray(camera, size=(160,120))
##time.sleep(0.1)

# Propiedades del video
##cap.set(3,160) #Width
##cap.set(4,120) #Height

# Imprime las propiedades de captura a consola
# for i in range(19):
# print(i, cap.get(i))


width = int(cap.get(3))
height = int(cap.get(4))

print(cap.get(3))
print(cap.get(4))

frameArea = height * width
minAreaTH = frameArea / 50
maxAreaTH = frameArea / 10
print('Area Threshold', minAreaTH)

# Lineas de entrada/salida
line_right = int(7 * (width / 10))  # RED
line_left = int(5 * (width / 10))  # BLUE
right_limit = int(7.5 * (width / 10))
left_limit = int(4.5 * (width / 10))

print("Red line x:", str(line_left))
print("Blue line x:", str(line_right))
line_left_color = (255, 0, 0)
line_right_color = (0, 0, 255)

# Substractor de fondo
# fgbg = cv.createBackgroundSubtractorMOG2(detectShadows=True)
# fgbg = cv.createBackgroundSubtractorMOG2(detectShadows=False, varThreshold=300)
fgbg = cv.createBackgroundSubtractorKNN(detectShadows=False, dist2Threshold=650)  # Best Result

# Elementos estructurantes para filtros morfologicos
kernelOp = np.ones((5, 5), np.uint8)
kernelOp2 = np.ones((7, 7), np.uint8)
kernelCl = np.ones((11, 11), np.uint8)

# Variables
font = cv.FONT_HERSHEY_SIMPLEX
persons = []
max_p_age = 5
pid = 1


def empyt(a):
    pass
# trackBars
cv.namedWindow("TrackBars")
cv.resizeWindow("TrackBars", 640, 340)
# cv.createButton()
cv.createTrackbar("Max Area", "TrackBars", int(maxAreaTH), height * width, empyt)
cv.createTrackbar("Min Area", "TrackBars", int(minAreaTH), height * width, empyt)
cv.createTrackbar("Left Limit", "TrackBars", left_limit, width, empyt)
cv.createTrackbar("Right Limit", "TrackBars", right_limit, width, empyt)
cv.createTrackbar("Left Line", "TrackBars", line_left, width, empyt)
cv.createTrackbar("Right Line", "TrackBars", line_right, width, empyt)

#Main Loop
while (cap.isOpened()):

    ##for image in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # Lee una imagen de la fuente de video
    ret, frame = cap.read()
    # frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    ##    frame = image.array

    for i in persons:
        i.age_one()  # age every person one frame
    #########################
    #   PRE-PROCESAMIENTO   #
    #########################

    # Aplica substraccion de fondo
    fgmask = fgbg.apply(frame)
    fgmask2 = fgbg.apply(frame)

    # Binariazcion para eliminar sombras (color gris)
    try:
        ret, imBin = cv.threshold(fgmask, 200, 255, cv.THRESH_BINARY)
        ret, imBin2 = cv.threshold(fgmask2, 200, 255, cv.THRESH_BINARY)
        # Opening (erode->dilate) para quitar ruido.
        mask = cv.morphologyEx(imBin, cv.MORPH_OPEN, kernelOp)
        mask2 = cv.morphologyEx(imBin2, cv.MORPH_OPEN, kernelOp)
        # Closing (dilate -> erode) para juntar regiones blancas.
        mask = cv.morphologyEx(mask, cv.MORPH_CLOSE, kernelCl)
        mask2 = cv.morphologyEx(mask2, cv.MORPH_CLOSE, kernelCl)
    except:
        print('EOF')
        print('UP:', cnt_right)
        print('DOWN:', cnt_left)
        break
    #################
    #   CONTORNOS   #
    #################

    # RETR_EXTERNAL returns only extreme outer flags. All child contours are left behind.
    contours0, hierarchy = cv.findContours(mask2, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

    for cnt in contours0:
        area = cv.contourArea(cnt)
        if minAreaTH < area < maxAreaTH:
            #################
            #   TRACKING    #
            #################

            # Falta agregar condiciones para multipersonas, salidas y entradas de pantalla.

            M = cv.moments(cnt)
            centerx = int(M['m10'] / M['m00'])
            centery = int(M['m01'] / M['m00'])
            x, y, w, h = cv.boundingRect(cnt)
            new = True
            if centerx in range(left_limit, right_limit):
                for i in persons:
                    if abs(x - i.getX()) <= w and abs(y - i.getY()) <= h:
                        # el objeto esta cerca de uno que ya se detecto antes
                        new = False
                        i.updateCoords(centerx, centery)  # actualiza coordenadas en el objeto and resets age
                        if i.going_RIGHT(line_right):
                            cnt_right += 1
                            cnt_inside += 1
                            print("ID:", i.getId(), 'crossed going right at', time.strftime("%c"))
                            log.write("ID: " + str(i.getId()) + ' crossed going right at ' + time.strftime("%c") + '\n')
                        elif i.going_LEFT(line_left):
                            cnt_left += 1
                            if cnt_inside > 0:
                                cnt_inside -= 1
                            print("ID:", i.getId(), 'crossed going left at', time.strftime("%c"))
                            log.write("ID: " + str(i.getId()) + ' crossed going left at ' + time.strftime("%c") + '\n')
                    if i.getState() == '1':
                        if i.getDir() == 'left' and i.getX() < left_limit:
                            i.setDone()
                    elif i.getDir() == 'right' and i.getX() > right_limit:
                        i.setDone()

                if new:
                    p = Person.MyPerson(pid, centerx, centery, max_p_age)
                    persons.append(p)
                    pid += 1

            #################
            #   DIBUJOS     #
            #################
            cv.circle(frame, (centerx, centery), 5, (255, 0, 0), -1)
            img = cv.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # cv.drawContours(frame, cnt, -1, (0,255,0), 3)

    # END for cnt in contours0

    #########################
    # DIBUJAR TRAYECTORIAS  #
    #########################
    for i in persons:
        if i.timedOut():
            index = persons.index(i)
            persons.pop(index)
            print('removed')
            del i  # liberar la memoria de i

        elif len(i.getTracks()) >= 2:
            pts = np.array(i.getTracks(), np.int32)
            pts = pts.reshape((-1, 1, 2))
            frame = cv.polylines(frame, [pts], False, i.getRGB())
            cv.putText(frame, str(i.getId()), (i.getX() + 10, i.getY() + 10), font, 0.3, i.getRGB(), 2, cv.LINE_AA)
            cv.putText(frame, str(i.getDirection()), (i.getX() + 10, i.getY() + 10), font, 0.3, i.getRGB(), 1,
                       cv.LINE_AA)
    # if i.getId() == 9:
    #     print(str(i.getX()), ',', str(i.getY())

    #################
    #   IMAGANES    #
    #################
    str_up = 'IN: ' + str(cnt_right)
    str_down = 'OUT: ' + str(cnt_left)
    str_inside = 'INSIDE: ' + str(cnt_inside)

    minAreaTH = cv.getTrackbarPos("Min Area", "TrackBars")
    maxAreaTH = cv.getTrackbarPos("Max Area", "TrackBars")
    left_limit = cv.getTrackbarPos("Left Limit", "TrackBars")
    right_limit = cv.getTrackbarPos("Right Limit", "TrackBars")
    line_right = cv.getTrackbarPos("Right Line", "TrackBars")
    line_left = cv.getTrackbarPos("Left Line", "TrackBars")

    # right Line
    pt1 = [line_right, 0]  # x,y pt1 Up
    pt2 = [line_right, height]  # x,y pt2 Down
    pts_L1 = np.array([pt1, pt2], np.int32)
    pts_L1 = pts_L1.reshape((-1, 1, 2))

    # Left Line
    pt3 = [line_left, 0]
    pt4 = [line_left, height]
    pts_L2 = np.array([pt3, pt4], np.int32)
    pts_L2 = pts_L2.reshape((-1, 1, 2))

    # Right_Limit
    pt5 = [right_limit, 0]
    pt6 = [right_limit, height]
    pts_L3 = np.array([pt5, pt6], np.int32)
    pts_L3 = pts_L3.reshape((-1, 1, 2))

    # Left_Limit
    pt7 = [left_limit, 0]
    pt8 = [left_limit, height]
    pts_L4 = np.array([pt7, pt8], np.int32)
    pts_L4 = pts_L4.reshape((-1, 1, 2))

    frame = cv.polylines(frame, [pts_L1], False, line_left_color, thickness=3)
    frame = cv.polylines(frame, [pts_L2], False, line_right_color, thickness=3)
    frame = cv.polylines(frame, [pts_L3], False, (255, 255, 255), thickness=3)
    frame = cv.polylines(frame, [pts_L4], False, (255, 255, 255), thickness=3)
    cv.putText(frame, str_up, (10, 20), font, 0.5, (255, 255, 255), 2, cv.LINE_AA)
    cv.putText(frame, str_up, (10, 20), font, 0.5, (0, 0, 255), 1, cv.LINE_AA)
    cv.putText(frame, str_down, (10, 40), font, 0.5, (255, 255, 255), 2, cv.LINE_AA)
    cv.putText(frame, str_down, (10, 40), font, 0.5, (255, 0, 0), 1, cv.LINE_AA)
    cv.putText(frame, str_inside, (10, 60), font, 0.5, (255, 255, 255), 2, cv.LINE_AA)
    cv.putText(frame, str_inside, (10, 60), font, 0.5, (0, 255, 0), 1, cv.LINE_AA)

    cv.imshow('Frame', frame)
    # cv.imshow('Mask', mask2)

    ##    rawCapture.truncate(0)
    # preisonar ESC para salir
    k = cv.waitKey(30) & 0xff
    if k == 27:
        break
    if k == 114 & 0xff:
        cnt_right = 0
        cnt_left = 0
        cnt_inside = 0
# END while(cap.isOpened())

#################
#   LIMPIEZA    #
#################
log.flush()
log.close()
cap.release()
cv.destroyAllWindows()
