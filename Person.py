from random import randint
import time


class MyPerson:
    tracks = []

    def __init__(self, i, xi, yi, max_age):
        self.i = i
        self.x = xi
        self.y = yi
        self.tracks = []
        self.R = randint(0, 255)
        self.G = randint(0, 255)
        self.B = randint(0, 255)
        self.done = False
        self.state = '0'
        self.age = 0
        self.max_age = max_age
        self.dir = None
        self.direction = 0

    def getRGB(self):
        return self.R, self.G, self.B

    def getTracks(self):
        return self.tracks

    def getId(self):
        return self.i

    def getState(self):
        return self.state

    def getDir(self):
        return self.dir

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getDirection(self):
        return self.direction

    def updateCoords(self, xn, yn):
        self.age = 0
        self.tracks.append([self.x, self.y])
        self.y = yn
        self.x = xn
        if len(self.tracks) > 2:
            self.direction += self.tracks[-1][0] - self.tracks[-2][0]

    def setDone(self):
        self.done = True

    def timedOut(self):
        return self.done

    def going_UP(self, up_limit):
        if len(self.tracks) >= 2:
            if self.state == '0':
                if self.tracks[-1][1] < up_limit <= self.tracks[-2][1]:  # cruzo la linea
                    state = '1'
                    self.dir = 'up'
                    return True
            else:
                return False
        else:
            return False

    def going_DOWN(self, down_limit):
        if len(self.tracks) >= 2:
            if self.state == '0':
                if self.tracks[-1][1] > down_limit >= self.tracks[-2][1]:  # cruzo la linea
                    state = '1'
                    self.dir = 'down'
                    return True
            else:
                return False
        else:
            return False

    def going_LEFT(self, left_limit):
        if len(self.tracks) >= 2:
            if self.state == '0':
                if self.tracks[-1][0] < left_limit <= self.tracks[-2][0]:  # cruzo la linea
                    self.state = '1'
                    self.dir = 'left'
                    return True
            else:
                return False
        else:
            return False

    def going_RIGHT(self, right_limit):
        if len(self.tracks) >= 2:
            if self.state == '0':
                if self.tracks[-1][0] > right_limit >= self.tracks[-2][0]:  # cruzo la linea
                    self.state = '1'
                    self.dir = 'right'
                    return True
            else:
                return False
        else:
            return False

    def age_one(self):
        self.age += 1
        if self.age > self.max_age:
            self.done = True
        return True


class MultiPerson:
    def __init__(self, persons, xi, yi):
        self.persons = persons
        self.x = xi
        self.y = yi
        self.tracks = []
        self.R = randint(0, 255)
        self.G = randint(0, 255)
        self.B = randint(0, 255)
        self.done = False
