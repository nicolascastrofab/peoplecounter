# PeopleCounter
Basic People Counter using Python and OpenCV

Install anaconda
pip install opencv-python

python PeopleCounter.py
# Projeto usado como base: https://github.com/Gupu25/PeopleCounter
# Feita implementação da UI para calibração dos limites de contagem e alteração da fonte de imagem
# Project by FabLAB FACENS
